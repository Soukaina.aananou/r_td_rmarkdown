---
title: "r_td_markdown"
output: html_document
date: '2022-10-15'
author: 'Soukaina AANANOU'
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown

TD sur l'utilisation des graphiques sur R. On retrouvera ci-dessous, le code pour lancer chaque graphique avec leur interpretation.

# Packages

```{r packages, warning=FALSE, include=FALSE}
library(dplyr)
library(ggplot2)
library(knitr)
library(stringr)
library(hrbrthemes)
```

```{r data, echo=FALSE, warning=FALSE, cache=TRUE}
path_import = "/Users/aananousoukaina/Desktop/M1 DSS/R/r_td2/exercice"

data_src = read.csv2(file.path(path_import, "data_220929.csv"))
data_src$sexe = factor(data_src$sexe)
data_src$asa = factor(data_src$asa)
levels(data_src$asa) = c("ASA1", "ASA2", "ASA3", "ASA4", "ASA5")

data = data_src
```

#Q1: Histogramme du poids
```{r}
data %>%
  filter(poids < 200) %>%
  ggplot(aes(x=poids))+
  geom_histogram(aes(y=..density..))
```
Nous avons une population avec une densité de poids variant notamment de 0 à 150kg. Nous avons un pic autour de 75kg et une majorité de patient avec un poids inclut surtout dans l'intervalle [50-100] kg.

#Q2: Histogramme du poids avec un axe des absisses comprenant les valeurs entre 0 et 150
```{r}
hist(data$poids, breaks = 150)

data %>%
  filter(poids < 150) %>%
  ggplot(aes(x=poids))+
  geom_histogram(aes(y=..density..))
  xlim = 150
```

# Q4 : Courbe de densité du poids en fonction du sexe
# supprimer au préalable les poids > 200
```{r}
data %>%
  filter(poids < 200) %>%
  ggplot(aes(x=poids, color=sexe))+
  geom_density()
```
Le poids moyen des femmes est de 60kg environ (pic à ce niveau là) tandis que celui des hommes est à 80 environ (pic à ce niveau là)

#Q5: Q4 + thème avec un fond blanc
```{r}
data %>%
  filter(poids < 200) %>%
  ggplot(aes(x=poids, color=sexe))+
  theme_classic()+
  geom_density()

```

#Q6: Boxplots représentant la durée d'anesthésie par classe ASA
```{r}
data %>%
  filter(duree_anesthesie < 1000) %>%
  ggplot(aes(duree_anesthesie, asa)) +
  geom_boxplot() +
  coord_flip()
```

On voit ici que plus l'état de santé est mauvais (asa5) moins nous avons de valeurs extremes et la médiane est plus élevée. On peut constater que la durée d'anesthesie mediane de l'ASA4 est la plus petite et celle de l'ASA 5 est la plus importante. Ce qui parait logique. La médiane des 3 autres catégorie se superpose.

#Q7: Représentation du nombre d'intervention par classe d'âge : [0-17[, [18-34[, [35-54[, [55-74[, >74 :
```{r}
data %>%
  mutate(classe_age = cut(age, breaks = c(0, 18, 35, 55, 75, 100))) %>%
  ggplot(aes(y = classe_age)) +
  geom_bar() +
  coord_flip()
```

Nous remarquons que la population recevant le plus grand nombre d'interventions sont les 35-55 ans (un peu plus de de 2000 interventions) suivi de très près des 55-75 ans (environ 2000 interventions). La population 18-35ans quant à elle recoit quand même un grand nombre d'interventions (autour de 1700). La population plus jeune (moins de 17ans) ou plus vieille (plus de 74ans) ne subisse pas beaucoup d'interventions comparé à ces 3 autres classes d'ages cités au dessus notamment comparé à la 35-55ans.

#Q8: Q7 avec proposition de nouvelles couleurs
```{r}
data %>%
  mutate(classe_age = cut(age, breaks = c(0, 18, 35, 55, 75, 100))) %>%
  ggplot(aes(y = classe_age, fill = classe_age)) +
  geom_bar() +
  coord_flip() +
  scale_fill_manual(values = c("#CD5C5C", "#F08080", "#FA8072", "#E9967A", "#FFA07A"))
```

#Q9: Q8 avec titre du graphique "Nombre d'interventions par classe d'âge" Retirer les libellés sur axes des abscisses et des ordonnées 
```{r}
data %>%
  mutate(classe_age = cut(age, breaks = c(0, 18, 35, 55, 75, 100))) %>%
  ggplot(aes(y = classe_age, fill = classe_age)) +
  geom_bar() +
  coord_flip() +
  scale_fill_manual(values = c("#CD5C5C", "#F08080", "#FA8072", "#E9967A", "#FFA07A")) +
  xlab("") +
  ylab("") +
  ggtitle("Nombre d'interventions par classe d'âge")
```

#Q10: Représentation de la mortalité par sexe
```{r}
prop_mortalite_sexe = round(prop.table(table(data$sexe, data$deces_sejour_interv), 1) * 100, 2)
df_mortalite_sexe = as.data.frame(prop_mortalite_sexe) %>%
  filter(Var2 == 1) %>%
  rename(freq_deces = `Freq`,
         sexe = Var1)

df_mortalite_sexe %>%
  ggplot(aes(x = sexe, y = freq_deces)) +
  geom_bar(stat = "identity")
```

On constate que dans la population des personnes décédés, les hommes ont un nombre de deces légèrement plus élevé que celui des femmes pourtant nous avons moins d'hommes que de femmes dans notre population totale.

#Q11: Représentation de l'IMC en fonction de la classe ASA : IMC = Poids / Taille (m) ²
```{r}
data %>%
  filter(!is.na(poids) & !is.na(taille) & age > 15) %>%
  mutate(imc = round(poids / (taille/100*taille/100))) %>%
  filter(imc < 50 & imc > 10) %>%
  ggplot(aes(asa, imc)) +
  geom_boxplot()
```

Nous retrouvons dans ce boxplot encore de nombreuses valeurs extremes voire aberrantes notamment pour les classes 1,2,3. Cependant, nous pouvons remarquer qu'avec un IMC médian légérement superieur à 25 les patients auront un état de santé moyen (classe 2,3,4) puis avec un IMC bas inférieur à 25, nous remarquons que le patient a soit dans la majorité des cas un état de santé normal (classe asa1) soit un état grave (classe asa5 qui à la médiane la plus petite). La classe asa5 recoit une faible distribution.

#Q12: Représentation de la mortalité en fonction d'un IMC > 30 kg/m²
```{r}
data_filtre =data %>%
  filter(!is.na(poids) & !is.na(taille)) %>%
  mutate(imc = round(poids / (taille/100*taille/100))) %>%
  filter(imc < 50 & imc > 10) %>%
  mutate(imc_30 = ifelse(imc > 30, 1, 0))

prop_mortalite_imc = round(prop.table(table(data_filtre$imc_30, data_filtre$deces_sejour_interv), 1) * 100, 2)

df_mortalite_imc = as.data.frame(prop_mortalite_imc) %>%
  filter(Var2 == 1) %>%
  rename(freq_deces = `Freq`,
         imc_30 = Var1)

df_mortalite_imc %>%
  mutate(imc_30_label = sub (0 , "Non" , imc_30)) %>%
  mutate(imc_30_label = sub (1 , "Oui" , imc_30_label)) %>%
  ggplot(aes(imc_30_label, freq_deces)) +
  geom_bar(stat = "identity") +
  ylab("Fréquence de décès") + xlab("IMC > 30")
```

Nous remarquons que lorsque l'iMC est élevé c'est à dire supérieur à 30, nous avons un plus grand nombre de décés.
Nous pouvons dire que l'IMC a un leger impact sur la mortalité si l'on se fie au graphique.


#Q13: Réprésenter la mortalité en fonction de IMC > 30 kg/m² et categorie_asa
```{r}
data_filtre %>%
  group_by(categorie_asa, imc_30, deces_sejour_interv) %>%
  summarize(n = n(),
  )

prop_asa_imc_deces = round(prop.table(table(data_filtre$imc_30, 
                                            data_filtre$categorie_asa, 
                                            data_filtre$deces_sejour_interv), 1) * 100, 2)

df_asa_imc_deces = as.data.frame(prop_asa_imc_deces) %>%
  rename(freq_asa = `Freq`,
         imc_30 = Var1,
         'Categorie_ASA' = Var2) %>%
  filter(Var3 == 1)

df_asa_imc_deces %>%
  mutate(imc_30_label = sub (0 , "Non" , imc_30)) %>%
  mutate(imc_30_label = sub (1 , "Oui" , imc_30_label)) %>%
  ggplot(aes(imc_30_label, freq_asa, fill = Categorie_ASA)) +
  geom_bar(position="dodge", stat="identity") +
  ylab("Fréquence") + xlab("IMC > 30")
```

Avec un IMC inférieur à 30 et un bon état de santé, la mortalité sera plus faible que si nous avons un IMC supérieur à 30 et un bon état de santé. 
Nous remarquons une nette augmentation de la mortalité quand l'IMC est supérieur à 30 même si les patients ont un bon état de santé général. 
Avec un IMC inférieur à 30 et un mauvais état de santé, la mortalité sera plus élevée que si nous avons un IMC supérieur à 30 et un mauvais état de santé même si la différence est quand même très faible. 
Cependant, nous pouvons quand même dire que l'IMC a un réel impact sur la mortalité et que plus il sera élevé plus il pourra impacté l'individu même si à la base il a un bon état de santé génral.

#Durée de séjour en fonction des classes ASA 

```{r}
data %>%
  ggplot(aes(asa, duree_sejour_interv)) +
  ggtitle("Duree de sejour en fonction des classes ASA")+
  geom_boxplot()
```

Nous remarquons que quand l'état de santé est moyen à grave (classe 3 4 5) la durée de sejour médiane est beaucoup plus grande que pour les autres états de santé (aux alentours de 10 pour la plus grande médiane contre 3 pour la plus petite). la classe asa2 a la plus petite médiane. La classe asa4 a la plus grande médiane. Nous percevons des valeurs extremes pour les 3 premières classes mais pas pour les deux dernieres. La distribution pour la classe asa4 et asa5 est la plus grande. 

#Durée de séjour en fonction de la CATÉGORIE ASA et du sexe

```{r}
data_filtre %>%
  group_by(categorie_asa, sexe, duree_sejour_interv) %>%
  summarize(n = n(),
  )

prop_asa_sexe_duree = round(prop.table(table(data_filtre$sexe, 
                                            data_filtre$categorie_asa, 
                                            data_filtre$duree_sejour_interv), 1) * 100, 2)

df_asa_sexe_duree = as.data.frame(prop_asa_sexe_duree) %>%
  rename(freq_asa = `Freq`,
         sexe = Var1,
         'Categorie_ASA' = Var2) %>%
  filter(Var3 == 1)

df_asa_sexe_duree %>%
  mutate(sexe_label = sub (0 , "H" , sexe)) %>%
  mutate(sexe_label = sub (1 , "F" , sexe_label)) %>%
  ggplot(aes(sexe_label, freq_asa, fill = Categorie_ASA)) +
  geom_bar(position="dodge", stat="identity") +
  ylab("Fréquence dureée de sejour") + xlab("sexe")
```

Nous remarquons que plus la catégorie asa est faible 1 ou 2, plus le sejour sera long que ce soit pour les femmes ou pour les hommes. Lorsque les patients sont dans la catégorie ASA 3-4-5, le sejour est moins long ce qui peut être logique car on peut l'explique par le fait qu'ils sont dans un état plus grave et sont donc soit décédé rapidement, soit transféré en soin intensif/palliatif.
Comme je ne savais plus si vous vouliez par catégorie asa ou par classe asa, j'ai fait un graphique avec chaque variable (voir celui du dessous pour voir par classe). Je pensais que catégorie et classe était la même chose mais j'ai des valeurs différentes donc ??


#Durée de séjour en fonction de la CLASSE ASA et du sexe

```{r}
###prmeier essai 
prop_asa_sexe_duree_2 = 
  round(prop.table(table(data$duree_sejour_interv, data$asa, 
                         data$sexe), 1) * 100, 2)

df_asa_sexe_duree_2 = as.data.frame(prop_asa_sexe_duree_2) %>%
  rename(freq_asa = `Freq`,
         sexe = Var3, 
         classe_asa = Var2)


df_asa_sexe_duree_2 %>%
  ggplot(aes(classe_asa, freq_asa, fill = sexe)) +
  geom_bar(position="dodge", stat="identity") +
  ylab("Fréquence dureée de sejour") + xlab("Classe_asa")

#deuxieme essai

prop_asa_sexe_duree1 = round(prop.table(table(data$sexe, 
                                            data$asa, 
                                            data$duree_sejour_interv), 1) * 100, 2)

df_asa_sexe_duree1 = as.data.frame(prop_asa_sexe_duree1) %>%
  rename(freq_asa = `Freq`,
         sexe = Var1,
         'Classe_ASA' = Var2)

df_asa_sexe_duree1 %>%
  mutate(sexe_label = sub (0 , "H" , sexe)) %>%
  mutate(sexe_label = sub (1 , "F" , sexe_label)) %>%
  ggplot(aes(sexe_label, freq_asa, fill = Classe_ASA)) +
  geom_bar(position="dodge", stat="identity") +
  ylab("Fréquence dureée de sejour") + xlab("sexe")
```

Nous remarquons que plus l'état de santé sera mauvais, moins le séjour sera long ce qui parait logique car les patients dans la classe ASA5 sont dans un état mourrant donc reste très peu hospitalisé. 



